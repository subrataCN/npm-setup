import {Component, View} from "angular2/core";

@Component({
   selector: 'myMovies'
})

@View({
  template: `
  		<h1>Top Movies</h1>
  		<ul>
  		<li>Hitman</li>
  		<li>Man Of steel</li>
  		<li>Lucy</li>
  		<li>Batman vs Superman</li>
  		</ul>`,
      styleUrls: ['./app/style.css', '']
})

export class myMoviesComponent {
	}
} 