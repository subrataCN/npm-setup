import {Component, View} from "angular2/core";
import {RouteConfig, ROUTER_DIRECTIVES} from "angular2/router";
import {myshopcartComponent} from './component.index';
import {cartComponent} from './component.cart';

@Component({
   selector: 'myshop-cart',
})

@View({
	  template : `<router-outlet></router-outlet>`,
	  styleUrls: ['./app/css/myshopComponent.css'],
	  directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
	{
		path: '/',
		component: myshopcartComponent,
		name: 'Home'
	}
	{
		path: '/cart',
		component: cartComponent,
		name: 'MyFirstCart'
	}
])



export class myMainApp {}