import {Component, View} from "angular2/core";

@Component({
   selector: '.my-for',
})

@View({
	  template: `<h1>Welcome TO my Shop</h1>
	  <p>We have Some movies Dvd For sell</p><br>
	  <div *ngIf = "PremiumUser == true">Only for restricted user</div>
     <ul>
     <li *ngFor = "#movie of movieList , #i = index">{{i+1}} :{{movie}}<b (click)="addtocart(movie)">  +</b></li>

     </ul>
     <br>
     Recently added movie: {{recentadded}}<br>

     Cart :<br>
     <ul>
     <li *ngFor = "#item of cart">{{item}}</li>
     <ul>`

})

export class myforComponent {
	PremiumUser = false;
	recentadded = "None";
	cart = [];
	movieList = ['Golmal','Sholay','Agnipath','Rishtey'];
	   addtocart(movie){
		    this.recentadded = movie;
		    this.cart.push(movie);
		   	alert('Movie "'+movie +'" is Added to cart');
	   }
}