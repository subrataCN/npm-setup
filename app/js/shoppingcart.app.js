System.register(["angular2/core", "angular2/router", './component.index', './component.cart'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, component_index_1, component_cart_1;
    var myMainApp;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (component_index_1_1) {
                component_index_1 = component_index_1_1;
            },
            function (component_cart_1_1) {
                component_cart_1 = component_cart_1_1;
            }],
        execute: function() {
            myMainApp = (function () {
                function myMainApp() {
                }
                myMainApp = __decorate([
                    core_1.Component({
                        selector: 'myshop-cart',
                    }),
                    core_1.View({
                        template: "<router-outlet></router-outlet>",
                        styleUrls: ['./app/css/myshopComponent.css'],
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }),
                    router_1.RouteConfig([
                        {
                            path: '/',
                            component: component_index_1.myshopcartComponent,
                            name: 'Home'
                        },
                        {
                            path: '/cart',
                            component: component_cart_1.cartComponent,
                            name: 'MyFirstCart'
                        }
                    ]), 
                    __metadata('design:paramtypes', [])
                ], myMainApp);
                return myMainApp;
            }());
            exports_1("myMainApp", myMainApp);
        }
    }
});
//# sourceMappingURL=shoppingcart.app.js.map