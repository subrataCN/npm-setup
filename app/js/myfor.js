System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var myforComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            myforComponent = (function () {
                function myforComponent() {
                    this.PremiumUser = false;
                    this.recentadded = "None";
                    this.cart = [];
                    this.movieList = ['Golmal', 'Sholay', 'Agnipath', 'Rishtey'];
                }
                myforComponent.prototype.addtocart = function (movie) {
                    this.recentadded = movie;
                    this.cart.push(movie);
                    alert('Movie "' + movie + '" is Added to cart');
                };
                myforComponent = __decorate([
                    core_1.Component({
                        selector: '.my-for',
                    }),
                    core_1.View({
                        template: "<h1>Welcome TO my Shop</h1>\n\t  <p>We have Some movies Dvd For sell</p><br>\n\t  <div *ngIf = \"PremiumUser == true\">Only for restricted user</div>\n     <ul>\n     <li *ngFor = \"#movie of movieList , #i = index\">{{i+1}} :{{movie}}<b (click)=\"addtocart(movie)\">  +</b></li>\n\n     </ul>\n     <br>\n     Recently added movie: {{recentadded}}<br>\n\n     Cart :<br>\n     <ul>\n     <li *ngFor = \"#item of cart\">{{item}}</li>\n     <ul>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], myforComponent);
                return myforComponent;
            }());
            exports_1("myforComponent", myforComponent);
        }
    }
});
//# sourceMappingURL=myfor.js.map