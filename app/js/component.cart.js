System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var cartComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            cartComponent = (function () {
                function cartComponent() {
                    this.cart = JSON.parse(localStorage.getItem('cartItems'));
                }
                cartComponent.prototype.remove = function (index) {
                    var cart = localStorage.getItem('cartItems');
                    console.log(index);
                    var cart = cart.splice(1, 1);
                    console.log(cart);
                };
                cartComponent = __decorate([
                    core_1.Component({}),
                    core_1.View({
                        template: "<h1>Subrata's Shop</h1>\n\t  <div id=\"cart\">    \n          <h3>Cart Details:</h3>\n          <ul>\n           <li *ngFor = \"#item of cart, #i = index\" (click) = \"remove(i)\">{{item}}</li>\n          </ul>\n     </div>"
                    }), 
                    __metadata('design:paramtypes', [])
                ], cartComponent);
                return cartComponent;
            }());
            exports_1("cartComponent", cartComponent);
        }
    }
});
//# sourceMappingURL=component.cart.js.map